<?php

namespace App\Controller;

use App\Entity\Categoria;
use App\Entity\ListaPedidoProducto;
use App\Entity\Pedido;
use App\Entity\Producto;
use App\Entity\Usuario;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CargarDatosController extends AbstractController
{
    /**
     * @Route("/test", name="test")
     */
    public function indexTest() {
        return new Response("test ok");
    }
    /**
     * @Route("/cargarDatos", name="cargar_datos")
     */
    public function cargarDatos(){
        $entityManager = $this -> getDoctrine() -> getManager();

        $usuario1 = new Usuario();
        $usuario1->setNombre("Miguel");
        $usuario1->setApellidos("Alvarez Perez");
        $usuario1->setRoles(array('ROLE_ADMINISTRADOR', 'ROLE_USER'));
        $usuario1->setEmail("miguel@gmail.com");
        $usuario1->setPassword('$2y$12$RSeI2SQOW/cUUU0kmUXblewO.c9H25dKkNMA84PawgI1aA29PXkCW');

        $usuario2 = new Usuario();
        $usuario2->setNombre("Angel");
        $usuario2->setApellidos("Gonzalez");
        $usuario2->setRoles(array('ROLE_CLIENTE', 'ROLE_USER'));
        $usuario2->setEmail("angel@gmail.com");
        $usuario2->setPassword('$2y$12$d09RvX7c3rRhm/YgxxWn6uvy2SjdxSMLn8SIY.p13mlX7Y5mEJFzm');

        $usuario3 = new Usuario();
        $usuario3->setNombre("Javier");
        $usuario3->setApellidos("Calvo");
        $usuario3->setRoles(array('ROLE_CLIENTE', 'ROLE_USER'));
        $usuario3->setEmail("javier@gmail.com");
        $usuario3->setPassword('$2y$12$TGimmCd3wJgD9sIK55UIl.qmBItancHF9hyMfzw2WzyK1p4P9e8xa');

        $categoria1 = new Categoria();
        $categoria1 ->setNombre("Consolas");
        $categoria2 = new Categoria();
        $categoria2 ->setNombre("Videojuegos");
        $categoria3 = new Categoria();
        $categoria3 ->setNombre("Perifericos");

        $producto1 = new Producto();
        $producto1 ->setNombre("PlayStation4");
        $producto1 ->setPrecio(399);
        $producto1 ->setStock(20);
        $producto1 ->setCategoria($categoria1);
        $producto1 ->setPhotoUrl("PlayStation4.png");

        $producto2 = new Producto();
        $producto2 ->setNombre("XboxOne");
        $producto2 ->setPrecio(399);
        $producto2 ->setStock(  18);
        $producto2 ->setCategoria($categoria1);
        $producto2 ->setPhotoUrl("XboxOne.png");

        $producto3 = new Producto();
        $producto3 ->setNombre("Nintendo Switch");
        $producto3 ->setPrecio(225);
        $producto3 ->setStock(  15);
        $producto3 ->setCategoria($categoria1);
        $producto3 ->setPhotoUrl("nintendoSwitch.jpg");

        $producto4 = new Producto();
        $producto4 ->setNombre("Counter-Strike");
        $producto4 ->setPrecio(10.99);
        $producto4 ->setStock(30);
        $producto4 ->setCategoria($categoria2);
        $producto4 ->setPhotoUrl("csgo.png");

        $producto5 = new Producto();
        $producto5 ->setNombre("Escape From Tarkov");
        $producto5 ->setPrecio(59.99);
        $producto5 ->setStock(32);
        $producto5 ->setCategoria($categoria2);
        $producto5 ->setPhotoUrl("escapefromtarkov.png");

        $producto6 = new Producto();
        $producto6 ->setNombre("Rust");
        $producto6 ->setPrecio(34.99);
        $producto6 ->setStock(25);
        $producto6 ->setCategoria($categoria2);
        $producto6 ->setPhotoUrl("rust.png");

        $producto7 = new Producto();
        $producto7 ->setNombre("League Of Legends");
        $producto7 ->setPrecio(19.98);
        $producto7 ->setStock(50);
        $producto7 ->setCategoria($categoria2);
        $producto7 ->setPhotoUrl("lol-logo.png");

        $producto8 = new Producto();
        $producto8 ->setNombre("Mando PS4");
        $producto8 ->setPrecio(50);
        $producto8 ->setStock(24);
        $producto8 ->setCategoria($categoria3);
        $producto8 ->setPhotoUrl("mandoPS4.png");

        $producto9 = new Producto();
        $producto9 ->setNombre("Mando XboxOne");
        $producto9 ->setPrecio(50);
        $producto9 ->setStock(23);
        $producto9 ->setCategoria($categoria3);
        $producto9 ->setPhotoUrl("mandoXboxOne.png");

        $carrito1 = new ListaPedidoProducto();
        $carrito1 ->setUsuario($usuario3);
        $carrito1 ->setAbierto(true);

        $pedido1 = new Pedido();
        $pedido1->setUsuario($usuario3);
        $pedido1->setProducto($producto1);
        $pedido1->setUnidades(32);
        $pedido1->setListaPedidoProducto($carrito1);

        $pedido2 = new Pedido();
        $pedido2->setUsuario($usuario3);
        $pedido2->setProducto($producto3);
        $pedido2->setUnidades(11);
        $pedido2->setListaPedidoProducto($carrito1);

        $entityManager->persist($carrito1);
        $entityManager->persist($pedido1);
        $entityManager->persist($pedido2);

        $entityManager->persist($usuario1);
        $entityManager->persist($usuario2);
        $entityManager->persist($usuario3);

        $entityManager->persist($categoria1);
        $entityManager->persist($categoria2);
        $entityManager->persist($categoria3);

        $entityManager->persist($producto1);
        $entityManager->persist($producto2);
        $entityManager->persist($producto3);
        $entityManager->persist($producto4);
        $entityManager->persist($producto5);
        $entityManager->persist($producto6);
        $entityManager->persist($producto7);
        $entityManager->persist($producto8);
        $entityManager->persist($producto9);

        $entityManager -> flush();
        return new Response ('Datos cargador correctamente!!');

    }
}
