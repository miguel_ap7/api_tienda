<?php

namespace App\Controller;

use App\Entity\Categoria;
use App\Entity\Producto;
use App\Repository\CategoriaRepository;
use App\Repository\ProductoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;


class CategoriaController extends AbstractController
{
    /**
 * @var CategoriaRepository
 */


    public function __construct(CategoriaRepository $categoriaRepository){
        $this->CategoriaRepository = $categoriaRepository;
    }

    /**
     * @Route("/api/categoria/{id}", name="listarUnaCategoria", methods={"GET"})
     */
    public function getCategoria($id): Response
    {
        $categoria = $this->getDoctrine()->getRepository(Categoria::class)->findOneBy(['id' => $id]);



        if(!$categoria){
            $data = [
                'status' => 404,
                'errors' => "Categoria no encontrada",
            ];
            return new JsonResponse($data, 404);
        }else{
            $data = [
                'id' => $categoria->getId(),
                'nombre' => $categoria ->getNombre(),
            ];
        }


        return new JsonResponse($data, Response::HTTP_OK);
    }
    /**
     * @Route("/api/categoria/{id}", name="update_Categoria", methods={"PUT"})
     */
    public function update($id, Request $request): JsonResponse
    {
        $categoria = $this->CategoriaRepository->findOneBy(['id' => $id]);
        $data = json_decode($request->getContent(),true);
        if(!$categoria){
            $data = [
                'status' => 404,
                'errors' => "Categoria no encontrado",
            ];
            return new JsonResponse($data, 404);
        }else{
            empty($data['nombre']) ? true : $categoria->setNombre($data['nombre']);


            $updateCategoria = $this->CategoriaRepository->updateCategoria($categoria);
        }

        return new JsonResponse(['status'=> 'Categoria modificada!'], Response::HTTP_OK);
    }
    /**
     * @Route("/api/categoria", name="crear_Categoria", methods={"POST"})
     */
    public function categoria(Request $request): JsonResponse
    {
        try{
            $data = json_decode($request->getContent(),true);
            $nombre = $data['nombre'];

            if (empty($nombre)){
                throw new NotFoundHttpException('Se tiene que introducir todos los parametros');
            }
            $this->CategoriaRepository->crearCategoria($nombre);

            return new JsonResponse(['status'=> 'Categoria creado!'], Response::HTTP_CREATED);
        }catch (\Exception $e){
            $data = [
                'status' => 422,
                'errors' => "Datos no validos",
            ];
            return new JsonResponse($data, 422);

        }

    }

    /**
     * @Route("/api/categoria/{id}", name="borrar_Categoria", methods={"DELETE"})
     */
    public function delCategoria($id): JsonResponse
    {

        $categoria = $this->CategoriaRepository->findOneBy(['id' => $id]);
        if (!$categoria){
            $data = [
                'status' => 404,
                'errors' => "Categoria no encontrada",
            ];
            return new JsonResponse($data, 403);
        }

        $this->CategoriaRepository->borrarCategoria($categoria);

        return new JsonResponse(['status'=> 'Categoria borrada!'], Response::HTTP_OK);

    }



}
