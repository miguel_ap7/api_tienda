<?php


namespace App\Controller;


use App\Entity\Producto;
use App\Repository\ProductoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ProductoUnAuthController extends AbstractController
{
    /**
     * @var ProductoRepository
     */

    public function __construct(ProductoRepository $ProductoRepository){
        $this->ProductoRepository = $ProductoRepository;
    }

    /**
     * @Route("/uapi/producto", name="listarProductos", methods={"GET"})
     */
    public function getAllProducto(): JsonResponse
    {
        $productos = $this->getDoctrine()->getRepository(Producto::class)->findAll();
        /// $categoria = $this->getDoctrine()->getRepository(Categoria::class)->findAll();
        foreach ($productos as $producto){
            $data [] = [
                'id' => $producto->getId(),
                'nombre' => $producto ->getNombre(),
                'precio' => $producto->getPrecio(),
                'stock' => $producto->getStock(),
                'categoria' => $producto->getCategoria()->getNombre(),
                'photoUrl' => $this->getParameter("url_images").$producto->getPhotoUrl(),
            ];
        }


        return new JsonResponse($data, Response::HTTP_OK);
    }


}