<?php
namespace App\Controller;


use App\Entity\Categoria;
use App\Entity\ListaPedidoProducto;
use App\Entity\Pedido;
use App\Entity\Usuario;
use App\Repository\UsuarioRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


class UsuarioController extends AbstractController
{
    /**
     * @var UsuarioRepository
     */


    public function __construct(UsuarioRepository $usuarioRepository){
        $this->UsuarioRepository = $usuarioRepository;
    }

    /**
     * @Route("/api/usuarios", name="listarUsuarios", methods={"GET"})
     */
    public function getAllUsuarios(): JsonResponse
    {
        $usuarios = $this->getDoctrine()->getRepository(Usuario::class)->findAll();
        /// $categoria = $this->getDoctrine()->getRepository(Categoria::class)->findAll();
        foreach ($usuarios as $usuario){
            $data [] = [
                'id' => $usuario->getId(),
                'nombre' => $usuario ->getNombre(),
                'apellidos' => $usuario->getApellidos(),
                'email' => $usuario->getEmail(),
                'roles' => $usuario->getRoles(),
            ];
        }


        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/api/usuario", name="listarUnUsuario", methods={"GET"})
     */
    public function getUsuario(): Response
    {
        $usuario = $this->getUser();

        if(!$usuario){
            $data = [
                'status' => 404,
                'errors' => "Usuario no encontrado",
            ];
            return new JsonResponse($data, 404);
        }else{
            $data = [
                'id' => $usuario->getId(),
                'nombre' => $usuario ->getNombre(),
                'apellidos' => $usuario->getApellidos(),
                'email' => $usuario->getEmail(),
                'roles' => $usuario->getRoles(),
            ];
        }


        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/api/usuario/carrito", name="infoCarritoUsuario", methods={"GET"})
     */
    public function carritoAbierto(): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user= $this->getUser();
        //Obtener lista de carritos del usuario, recorrer lista buscando carrito abierto. Obtener id de carrito abierto y recuperarlo de la base de datos y devolverlo serializado y probarlo con la extension
        $listasPedidoProducto = $this->getDoctrine()->getRepository(ListaPedidoProducto::class)->findBy(array('usuario' => $user));
        $carritoEncontrado = false;
        foreach ($listasPedidoProducto as $listaPedidoProducto){
            if ($listaPedidoProducto->getAbierto()==true){
                $idCarrito = $listaPedidoProducto->getId();
                $carritoEncontrado=true;
            }
        }
        if ($carritoEncontrado){
            $carritoActual = $this->getDoctrine()->getRepository(ListaPedidoProducto::class)->findOneBy(['id' => $idCarrito]);
            return $this->enviaSerializado($carritoActual);
        }else{
            $response = new Response();
            $response->setStatusCode(404);
            return $response;
        }




    }
    public function enviaSerializado($objetoASerializar):Response{
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object;
            },
        ];

        $normalizer = new ObjectNormalizer(null, null, null, null, null, null, $defaultContext);

        // construimos o encoder; neste caso JSON
        $encoder= new JsonEncoder();

        // construimos o serializer co normalizer e encoder
        $serializer = new Serializer(array($normalizer), array($encoder));

        // serializamos en JSON
        $jsonContent =  $serializer->serialize($objetoASerializar, "json");

        // Construimos a resposta serializada en JSON
        $response = new Response();
        $response ->headers->set("Content-Type", "application/json");
        $response->setStatusCode(200);
        $response->setContent($jsonContent);

        // devolvemos a resposta
        return $response;

    }


}
