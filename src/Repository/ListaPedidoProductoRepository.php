<?php

namespace App\Repository;

use App\Entity\ListaPedidoProducto;
use App\Entity\Usuario;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method ListaPedidoProducto|null find($id, $lockMode = null, $lockVersion = null)
 * @method ListaPedidoProducto|null findOneBy(array $criteria, array $orderBy = null)
 * @method ListaPedidoProducto[]    findAll()
 * @method ListaPedidoProducto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ListaPedidoProductoRepository extends ServiceEntityRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, ListaPedidoProducto::class);
        $this->manager = $manager;
    }

    public function updateCarrito(ListaPedidoProducto $listaPedidoProducto): ListaPedidoProducto{
        $this->manager->persist($listaPedidoProducto);
        $this->manager->flush();

        return $listaPedidoProducto;
    }

    public function crearCarrito($idUsuario)
    {
        $newListaPedidoProducto = new ListaPedidoProducto();

        $usuario = $this -> manager -> getRepository(Usuario::class)->find($idUsuario);
        $newListaPedidoProducto ->setUsuario($usuario);

        $this->manager->persist($newListaPedidoProducto);
        $this->manager->flush();
    }


    public function borrarCarrito($listaPedidoProducto){
        $this->manager->remove($listaPedidoProducto);
        $this->manager->flush();
    }

    // /**
    //  * @return ListaPedidoProducto[] Returns an array of ListaPedidoProducto objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ListaPedidoProducto
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
