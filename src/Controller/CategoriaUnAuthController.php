<?php


namespace App\Controller;


use App\Entity\Categoria;
use App\Repository\CategoriaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoriaUnAuthController extends AbstractController
{
    /**
     * @var CategoriaRepository
     */


    public function __construct(CategoriaRepository $categoriaRepository){
        $this->CategoriaRepository = $categoriaRepository;
    }

    /**
     * @Route("/uapi/categoria", name="listarCategorias", methods={"GET"})
     */
    public function getAllCategorias(): JsonResponse
    {
        $categorias = $this->getDoctrine()->getRepository(Categoria::class)->findAll();
        foreach ($categorias as $categoria){
            $data [] = [
                'id' => $categoria->getId(),
                'nombre' => $categoria ->getNombre(),

            ];
        }


        return new JsonResponse($data, Response::HTTP_OK);
    }
}