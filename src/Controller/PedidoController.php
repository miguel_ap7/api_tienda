<?php

namespace App\Controller;

use App\Entity\Categoria;
use App\Entity\ListaPedidoProducto;
use App\Entity\Pedido;
use App\Entity\Producto;
use App\Entity\Usuario;
use App\Repository\PedidoRepository;
use App\Repository\ProductoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;


class PedidoController extends AbstractController
{
    /**
     * @var PedidoRepository
     */

    public function __construct(PedidoRepository $pedidoRepository){
        $this->PedidoRepository = $pedidoRepository;
    }

    /**
     * @Route("/api/pedido", name="listarPedidos", methods={"GET"})
     */
    public function getAllPedidos(): JsonResponse
    {
        $pedidos = $this->getDoctrine()->getRepository(Pedido::class)->findAll();

        foreach ($pedidos as $pedido){
            $data [] = [
                'id' => $pedido->getId(),
                'idProducto'=>$pedido->getProducto()->getId(),
                'idListaPedidoProducto'=>$pedido->getListaPedidoProducto()->getId(),
                'idUsuario'=>$pedido->getUsuario()->getId(),
                'unidades'=>$pedido->getUnidades()

            ];
        }


        return new JsonResponse($data, Response::HTTP_OK);
    }
    /**
     * @Route("/api/pedido/{id}", name="listarUnPedido", methods={"GET"})
     */
    public function getPedido($id): Response
    {
        $pedido = $this->getDoctrine()->getRepository(Pedido::class)->findOneBy(['id' => $id]);



        if(!$pedido){
            $data = [
                'status' => 404,
                'errors' => "Pedido no encontrado",
            ];
            return new JsonResponse($data, 404);
        }else{
            $data = [
                'id' => $pedido->getId(),
                'idProducto'=>$pedido->getProducto()->getId(),
                'idListaPedidoProducto'=>$pedido->getListaPedidoProducto()->getId(),
                'idUsuario'=>$pedido->getUsuario()->getId(),
                'unidades'=>$pedido->getUnidades()
            ];
        }


        return new JsonResponse($data, Response::HTTP_OK);
    }
    /**
     * @Route("/api/pedido/{id}", name="update_Pedido", methods={"PUT"})
     */
    public function update($id, Request $request): JsonResponse
    {
        $pedido = $this->PedidoRepository->findOneBy(['id' => $id]);
        $data = json_decode($request->getContent(),true);
        if(!$pedido){
            $data = [
                'status' => 404,
                'errors' => "Pedido no encontrado",
            ];
            return new JsonResponse($data, 404);
        }else{
            empty($data['idProducto']) ? true : $pedido->setProducto($this->getDoctrine()->getRepository(Producto::class)->find($data['idProducto']));
            empty($data['unidades']) ? true : $pedido->setUnidades($data['unidades']);

            $updateProducto = $this->PedidoRepository->updatePedido($pedido);
        }




        return new JsonResponse(['status'=> 'Pedido modificado!'], Response::HTTP_OK);
    }
    /**
     * @Route("/api/pedido", name="crear_Pedido", methods={"POST"})
     */
    public function pedido(Request $request): JsonResponse
    {
        try{
            $entityManager = $this->getDoctrine()->getManager();
            $data = json_decode($request->getContent(),true);
            $idProducto = $data['idProducto'];
            $unidades = $data['unidades'];
            $user = $this->getUser();

            $nuevaListaPedidoProducto = new ListaPedidoProducto();
            $productoSeleccionado = $this->getDoctrine()->getRepository(Producto::class)->find($idProducto);

            $pedido = new Pedido();
            $pedido->setProducto($productoSeleccionado);
            $pedido->setUnidades($unidades);
            $pedido->setUsuario($user);

            //$idListaPedidoProducto = $data['idListaPedidoProducto'];
            //$idUsuario = $data['idUsuario'];

            //Mirar si tiene carrito el usuario

            $listasPedidoProductoBD = $entityManager->getRepository(ListaPedidoProducto::class)->findBy(array('usuario'=> $user));

            $tieneCarritoAbierto=false;
            if(count($listasPedidoProductoBD)!=0){
                //Mirar si tiene carrito abierto
                foreach ($listasPedidoProductoBD as $carrito){
                    if ($carrito->getAbierto()){
                        $listaPedidoProductoBD = $carrito;
                        $tieneCarritoAbierto=true;
                    }
                }
            }
            //Si tiene carrito abierto mirar si tiene un pedido de ese producto.
            if ($tieneCarritoAbierto) {
                $productoRepetido = false;
                foreach ($listaPedidoProductoBD->getPedidos() as $pedidoBD) {
                    if ($pedidoBD->getProducto()->getId() == $idProducto) {
                        //Si lo tiene aumentar el numero de unidades
                        $pedidoBD->setUnidades($pedidoBD->getUnidades() + $unidades);
                        $productoRepetido = true;
                    }
                }

                if (!$productoRepetido) {
                    //Si no lo tiene el producto tenemos que crear el pedido del producto y lo metes en el carrito.
                    $listaPedidoProductoBD->addPedido($pedido);
                }
                $entityManager->persist($listaPedidoProductoBD);
                $entityManager->flush();
            }else{
                //Si no tiene carrito debemos crear el carrito, el pedido y meter el pedido en el carrito.
                $nuevaListaPedidoProducto->setUsuario($user);
                $nuevaListaPedidoProducto->setAbierto(true);
                $nuevaListaPedidoProducto->addPedido($pedido);
                $entityManager->persist($nuevaListaPedidoProducto);
                $entityManager -> flush();
            }

            //if (empty($idProducto) || empty($idListaPedidoProducto) || empty($idUsuario) || empty($unidades) ){
            //    throw new NotFoundHttpException('Se tiene que introducir todos los parametros');
            //}
            //$this->PedidoRepository->crearPedido($idProducto, $idListaPedidoProducto, $idUsuario, $unidades);

            return new JsonResponse(['status'=> 'Pedido creado!'], Response::HTTP_CREATED);
        }catch (\Exception $e){
            $data = [
                'status' => 422,
                'errors' => "Datos no validos",
            ];
            return new JsonResponse($data, 422);

        }

    }

    /**
     * @Route("/api/pedido/{id}", name="borrar_Pedido", methods={"DELETE"})
     */
    public function delPedido($id): JsonResponse
    {

        $pedido = $this->PedidoRepository->findOneBy(['id' => $id]);
        if (!$pedido){
            $data = [
                'status' => 404,
                'errors' => "Producto no encontrado",
            ];
            return new JsonResponse($data, 403);
        }

        $this->PedidoRepository->borrarPedido($pedido);

        return new JsonResponse(['status'=> 'Pedido borrado!'], Response::HTTP_OK);

    }



}
