<?php

namespace App\Repository;

use App\Entity\Categoria;
use App\Entity\Producto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method Producto|null find($id, $lockMode = null, $lockVersion = null)
 * @method Producto|null findOneBy(array $criteria, array $orderBy = null)
 * @method Producto[]    findAll()
 * @method Producto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductoRepository extends ServiceEntityRepository
{

    /**
     * @var EntityManagerInterface
     */
    private $manager;
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Producto::class);
        $this->manager = $manager;
    }

    public function crearProducto($nombre, $precio, $stock, $idCategoria)
    {
        $newProducto = new Producto();
        $newProducto
            ->setNombre($nombre)
            ->setPrecio($precio)
            ->setStock($stock);
        $categoria = $this -> manager -> getRepository(Categoria::class)->find($idCategoria);
        $newProducto ->setCategoria($categoria);
        $this->manager->persist($newProducto);
        $this->manager->flush();
    }

    public function updateProducto(Producto $producto): Producto{
        $this->manager->persist($producto);
        $this->manager->flush();

        return $producto;
    }

    public function borrarProducto($producto){
        $this->manager->remove($producto);
        $this->manager->flush();
    }



// /**
    //  * @return Producto[] Returns an array of Producto objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Producto
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}