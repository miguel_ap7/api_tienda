var token=null;
$(document).ready (function (){

        if (token==null){
            cargarProductos();
            cargarCategorias();
            $("#login").click(function (){login()});
            $("#register").click(function (){register()});
            $("#logout").click(function (){logout()});
            $("#inicio").click(function (){volInicio()});
            $("#logout").hide();
            $("#addProducto").hide();
            $("#addCategoria").hide();
        }


    }
);
function login() {
    var formularioLogin;
    formularioLogin = "<form id='productos'>" +
        "<p id='error'></p>" +
        "<label>Email: </label><input type='email' id='campoEmail'>" +
        "<label>Contraseña: </label><input type='password' id='campoPassword'>" +
        "<input type='button' id='botonLogin' name='botonLogin' value='Iniciar Sesion' >" +
        "</form>";


    $("#productos").replaceWith(formularioLogin);
    $("#botonLogin").click(function (){
        var email = $("#campoEmail").val();
        var password = $("#campoPassword").val();
        iniciar(email, password);
    });
}
function register() {
    var formularioRegistro;
    formularioRegistro = "<form id='productos'>" +
        "<p id='error'></p>" +
        "<table><tr><td><label>Nombre: </label></td><td><input type='text' id='campoNombre'></td>"+
        "<td><label>Apellidos: </label></td><td><input type='text' id='campoApellidos'></td></tr>"+
        "<tr><td><label>Email: </label></td><td><input type='email' id='campoEmail'></td>"+
        "<td><label>Contraseña: </label></td><td><input type='password' id='campoPassword'></td>"+
        "</tr></table><br><br>" +
        "<input type='button' id='botonRegistro' name='botonRegistro' value='Crear cuenta' >" +
        "</form>";


    $("#productos").replaceWith(formularioRegistro);
    $("#botonRegistro").click(function (){
        var nombre = $("#campoNombre").val();
        var apellidos = $("#campoApellidos").val();
        var email = $("#campoEmail").val();
        var password = $("#campoPassword").val();
        registro(email, password, nombre, apellidos);
    });
}
function logout() {
    location.reload();
}
function volInicio() {
    var vacio = "<div class=\"w3-row w3-grayscale\" id=\"productos\"></div>";
    $("#productos").replaceWith(vacio);
    cargarProductos();
}
function registro(email, password, nombre, apellidos) {
    $.ajax(
        {
            url: "http://api_tienda.local/register",
            type: "POST",
            async: true,
            contentType: 'application/json',
            data: JSON.stringify({  email: email, password: password, nombre:nombre, apellidos:apellidos  }),
            dataType: "json",
            success: function () {
                alert("Usuario registrado correctamente");
                location.reload();

            },

        }
    );


}
function iniciar(email, password) {
    $.ajax(
        {
            url: "http://api_tienda.local/api/login_check",
            type: "POST",
            async: true,
            contentType: 'application/json',
            data: JSON.stringify({  username: email,   password: password  }),
            dataType: "json",
            success: function (dataToken) {
                console.log(dataToken);
                token = dataToken.token;
                datosUsuario();


            },
            error: function () {
                alert("Usuario o contraseña erroneos, vuelve a intentarlo")
            }

        }
    );

return token;

}




function cargarProductos() {
    $.ajax(
        {
            url: "http://api_tienda.local/uapi/producto",
            type: "GET",
            async: true,
            data: {},
            headers: {},
            dataType: "json",
            success: function (listaProductos) {
                console.log(listaProductos);
                var divProductos;
                $.each(listaProductos, function () {
                    divProductos = "<div class=\"w3-col l3 s6 \"><div class=\"w3-bar-item w3-button divProductos \"  id="+this.id+"><div class=\"w3-container \" id=\"photo\"><img src="+this.photoUrl+">"+"</div><div class=\"producto\" ><p>"+this.nombre+"<br><b>"+this.precio+"€</b></p></div></a></div>";


                    $("#productos").append(divProductos);
                    $("#"+this.id).click(function (){hacerPedido(this.id)});
                });
                infoCarritoActual();


            }
        }
    );

}
function cargarCategorias(){

    $.ajax(
        {
            url: "http://api_tienda.local/uapi/categoria",
            type: "GET",
            async: true,
            data: {},
            headers: {},
            dataType: "json",
            success: function (listaCategorias) {
                console.log(listaCategorias);


                var listaCat;
                $.each(listaCategorias, function () {
                    listaCat = "<div><a class=\"w3-bar-item w3-button\">"+this.nombre+"</a></div>";
                    $("#listaCategorias").append(listaCat);

                });

            }
        }
    );
}

function datosUsuario() {
    var cabecera = 'Bearer '+token;
    $.ajax(
        {
            url: "http://api_tienda.local/api/usuario",
            type: "GET",
            async: true,
            data: {},
            headers: {'Authorization': cabecera },
            dataType: "json",
            success: function (listaDatosUser) {
                var datos;
                    datos = listaDatosUser.nombre;
                    console.log(listaDatosUser.roles);
                    //$("#usuario").append(datos);
                var vacio = "<div class=\"w3-row w3-grayscale\" id=\"productos\"></div>";
                $.each(listaDatosUser.roles, function (index, element) {
                  if(element === "ROLE_CLIENTE"){
                      $("#productos").replaceWith(vacio);
                      cargarProductos();
                      $("#login").hide();
                      $("#register").hide();
                      $("#logout").show();
                      $("#nombreUser").append(listaDatosUser.nombre);


                  }else if(element === "ROLE_ADMINISTRADOR"){
                      $("#productos").replaceWith(vacio);
                      cargarProductos();
                      $("#login").hide();
                      $("#register").hide();
                      $("#logout").show();
                      $("#addCategoria").show();
                      $("#addProducto").show();
                      $("#nombreUser").append(listaDatosUser.nombre);
                      $("#addCategoria").click(function (){formularioCategoriaNueva()});
                      $("#addProducto").click(function (){formularioProductoNuevo()});

                  }
                });

            }
        }
    );
}
function formularioProductoNuevo(){
    var vacio = "<div class=\"w3-row w3-grayscale\" id=\"productos\"></div>";
    $("#productos").replaceWith(vacio);
    $("#productos").replaceWith("<div id='productos'> <table><tr><td>Nombre</td><td><input type='text' id='campoNombreProdNew'></td></tr>" +
        "<tr><td>Precio</td><td><input type='number' id='campoPrecioProdNew'></td></tr>"+
        "<tr><td>Stock</td><td><input type='number' id='campoStockProdNew'></td></tr>"+
        "<tr><td>Categoria</td><td><select id='selectCatNewProd'></select></td></tr>"+
        "</table>"+
        "<input type='button' id='botonAñadirProd' name='botonAñadirProd' value='Añadir nuevo producto' ></div>"

    );
    listaCatNewProd();
    $("#selectCatNewProd").change(function(){
        valorNewProducto = $(this).val();

    });

    $("#botonAñadirProd").click(function (){
        var nombre = $("#campoNombreProdNew").val();
        var precio = $("#campoPrecioProdNew").val();
        var stock = $("#campoStockProdNew").val();
        var valorSelectNewProducto = valorNewProducto;
        crearProducto(nombre, precio, stock, valorNewProducto);
    });
}
function crearProducto(nombre, precio, stock, categoria) {
    var cabecera = 'Bearer '+token;
    $.ajax(
        {
            url: "http://api_tienda.local/api/producto",
            type: "POST",
            async: true,
            data: JSON.stringify({_method: 'POST', nombre:nombre, precio:precio, stock:stock, categoria: categoria}),
            headers: {'Authorization': cabecera },
            dataType: "json",
            success: function (datosNewProducto) {
                alert("Producto añadido con exito");
                var vacio = "<div class=\"w3-row w3-grayscale\" id=\"productos\"></div>";
                $("#productos").replaceWith(vacio);
                cargarProductos();

            }
        }
    );
}

function listaCatNewProd(){
    $.ajax(
        {
            url: "http://api_tienda.local/uapi/categoria",
            type: "GET",
            async: true,
            data: {},
            headers: {},
            dataType: "json",
            success: function (listaCategorias) {
                console.log(listaCategorias);
                $.each(listaCategorias, function () {
                    $("#selectCatNewProd").append("<option value="+this.id+">"+this.nombre+"</option>");
                });
                //console.log(categoriaProd);
                //$("#selectCategorias option:contains("+categoriaProd+")").attr("selected", true);


            }
        }

    );

}
function formularioCategoriaNueva(){
    var formularioAddCategoria;
    formularioAddCategoria = "<form id='productos'>" +
        "<label>Nombre de la nueva categoria: </label><input type='text' id='campoNombreCategoria'>" +
        "<input type='button' id='botonCrearCategoria' name='botonCrearCategoria' value='Crear' >" +
        "</form>";


    $("#productos").replaceWith(formularioAddCategoria);
    $("#botonCrearCategoria").click(function (){
        var nombreCategoria="";
        nombreCategoria = $("#campoNombreCategoria").val();
        crearCategoria(nombreCategoria);
    });

}


function crearCategoria(nombreCategoria) {
    var cabecera = 'Bearer '+token;
    $.ajax(
        {
            url: "http://api_tienda.local/api/categoria",
            type: "POST",
            async: true,
            data: JSON.stringify({_method: 'POST', nombre:nombreCategoria}),
            headers: {'Authorization': cabecera },
            dataType: "json",
            success: function (datosNuevaCategoria) {
                alert("Categoria creada con exito");
                var vacio = "<div class=\"w3-row w3-grayscale\" id=\"productos\"></div>";
                $("#productos").replaceWith(vacio);


            }
        }
    );
}




function hacerPedido(id) {
    var cabecera = 'Bearer '+token;
    //console.log(id);
    $.ajax(
        {
            url: "http://api_tienda.local/api/producto/"+id,
            type: "GET",
            async: true,
            data: {},
            headers: {'Authorization': cabecera },
            dataType: "json",
            success: function (datosProducto) {
                console.log(datosProducto);
                $.ajax(
                    {
                        url: "http://api_tienda.local/api/usuario",
                        type: "GET",
                        async: true,
                        data: {},
                        headers: {'Authorization': cabecera },
                        dataType: "json",
                        success: function (listaDatosUser) {
                            var datos;
                            datos = listaDatosUser.nombre;
                            console.log(listaDatosUser.roles);
                            //$("#usuario").append(datos);
                            var vacio = "<div class=\"w3-row w3-grayscale\" id=\"productos\"></div>";
                            $.each(listaDatosUser.roles, function (index, element) {
                                if(element === "ROLE_CLIENTE"){
                                    var producto;
                                    producto = "<form id='productos'><div  id=\"imgProducto\" id="+datosProducto.id+"><img src="+datosProducto.photoUrl+"  >"+"</div><div  id=\"textProducto\"><p><b>Nombre: </b>"+datosProducto.nombre+"<br><b>Precio: </b>"+datosProducto.precio+"€"+"<br><b>Categoria: </b>"+
                                        datosProducto.categoria+"<br><b>Stock disponible: </b>"+datosProducto.stock+"<br><b>Unidades: </b>" + "<input type='number' id='unidadesP' min='0' value='0'><br> <input type='button' id='botonCompra' name='botonCompra' value='Comprar producto' > </p></div></form>";
                                    //<button id='botonCompra'>Comprar Producto</button>

                                    $("#productos").replaceWith(producto);
                                    $("#botonCompra").click(function (){añadirPedido(datosProducto.id, $("#unidadesP").val())});


                                }else if(element === "ROLE_ADMINISTRADOR"){
                                    var producto;
                                    producto = "<form id='productos'><div  id=\"imgProducto\" id="+datosProducto.id+"><img src="+datosProducto.photoUrl+"  >"+"</div><div  id=\"textProducto\"><p><b>Nombre: </b>"+datosProducto.nombre+"<br><b>Precio: </b>"+datosProducto.precio+"€"+"<br><b>Categoria: </b>"+
                                        datosProducto.categoria+"<br><b>Stock disponible: </b>"+datosProducto.stock+"<br><b>Unidades: </b>" + "<input type='number' id='unidadesP' min='0' value='0'><br> <input type='button' id='botonCompra' name='botonCompra' value='Comprar producto' ><input type='button' id='botonModificar' name='botonModificar' value='Modificar producto' ><input type='button' id='botonBorrarProd' name='botonBorrarProd' value='Borrar producto' > </p></div></form>";
                                    //<button id='botonCompra'>Comprar Producto</button>

                                    $("#productos").replaceWith(producto);
                                    $("#botonCompra").click(function (){añadirPedido(datosProducto.id, $("#unidadesP").val())});
                                    $("#botonModificar").click(function (){hacerModificacion(datosProducto.id);});
                                    $("#botonBorrarProd").click(function (){borrarProdConfir(datosProducto.id);});

                                }
                            });

                        }
                    }
                );


            }
        }
    );
}
function borrarProdConfir (idProd){
    var opcion = confirm("¿Quiere borrar este producto?");
    if(opcion===true){
        borrarProducto(idProd);
    }
}
function borrarProducto(idProd){
    var cabecera = 'Bearer '+token;
    $.ajax(
        {
            url: "http://api_tienda.local/api/producto/"+idProd,
            type: "POST",
            async: true,
            data: {_method: 'delete'},
            headers: {'Authorization': cabecera },
            dataType: "json",
            success: function () {
                alert("Producto borrado con exito")
                var vacio = "<div class=\"w3-row w3-grayscale\" id=\"productos\"></div>";
                $("#productos").replaceWith(vacio);
                cargarProductos();

            }
        }

    );

}


function listaCategorias(categoriaProd){
    $.ajax(
        {
            url: "http://api_tienda.local/uapi/categoria",
            type: "GET",
            async: true,
            data: {},
            headers: {},
            dataType: "json",
            success: function (listaCategorias) {
                console.log(listaCategorias);
                $.each(listaCategorias, function () {
                    $("#selectCategorias").append("<option value="+this.id+">"+this.nombre+"</option>");
                });
                //console.log(categoriaProd);
                $("#selectCategorias option:contains("+categoriaProd+")").attr("selected", true);
            }
        }

    );

}
function hacerModificacion(idProduProd){
    var cabecera = 'Bearer '+token;
    //console.log(idProductoP);
    //console.log(unidades);
    $.ajax(
        {
            url: "http://api_tienda.local/api/producto/"+idProduProd,
            type: "GET",
            async: true,
            data: {},
            headers: {'Authorization': cabecera },
            dataType: "json",
            success: function (datosProducto) {
                var vacio = "<div class=\"w3-row w3-grayscale\" id=\"productos\"></div>";

                $("#productos").replaceWith(vacio);
                $("#productos").replaceWith("<div id='productos'> <table><tr><td>Nombre</td><td><input type='text' id='campoNombreProd'></td></tr>" +
                    "<tr><td>Precio</td><td><input type='number' id='campoPrecioProd'></td></tr>"+
                    "<tr><td>Stock</td><td><input type='number' id='campoStockProd'></td></tr>"+
                    "<tr><td>Categoria</td><td><select id='selectCategorias'></select></td></tr>"+
                    "</table>"+
                    "<input type='button' id='botonModificarProd' name='botonModificarProd' value='Modificar' ></div>"

                );
                listaCategorias(datosProducto.categoria);



                $("#campoNombreProd").val(datosProducto.nombre);
                $("#campoPrecioProd").val(datosProducto.precio);
                $("#campoStockProd").val(datosProducto.stock);
                $("#selectCategorias").change(function(){
                    valor = $(this).val();

                });
                $("#botonModificarProd").click(function (){
                    var nombre = $("#campoNombreProd").val();
                    var precio = $("#campoPrecioProd").val();
                    var stock = $("#campoStockProd").val();
                    var valorSelect = valor;
                    modificarProducto(datosProducto.id, nombre, precio, stock, valor);
                });


            }
        }
    );
}
function modificarProducto(idProd, nombre, precio, stock, categoria) {
    var cabecera = 'Bearer '+token;
    //console.log(idProductoP);
    //console.log(unidades);
    $.ajax(
        {
            url: "http://api_tienda.local/api/producto/"+idProd,
            type: "PUT",
            async: true,
            data: JSON.stringify({ _method: 'PUT', nombre:nombre, precio:precio, stock:stock, idCategoria:categoria}),
            headers: {'Authorization': cabecera },
            dataType: "json",
            success: function (datosProducto) {
                alert("Se modificaron los datos de producto correctamente.");
                var vacio = "<div class=\"w3-row w3-grayscale\" id=\"productos\"></div>";
                $("#productos").replaceWith(vacio);
                cargarProductos();




            }
        }
    );
}
function añadirPedido(idProd, uProd){
    var cabecera = 'Bearer '+token;
    //console.log(idProductoP);
    //console.log(unidades);
    $.ajax(
        {
            url: "http://api_tienda.local/api/pedido",
            type: "POST",
            async: true,
            data: JSON.stringify({ _method: 'POST', idProducto:idProd, unidades:uProd}),
            headers: {'Authorization': cabecera },
            dataType: "json",
            success: function (datosPedido) {
                alert("El producto se añadio al carrito.");
                var vacio = "<div class=\"w3-row w3-grayscale\" id=\"productos\"></div>";
                $("#productos").replaceWith(vacio);
                cargarProductos();


            }
        }
    );
}

function infoCarritoActual() {
    var cabecera = 'Bearer '+token;
    $.ajax(
        {
            url: "http://api_tienda.local/api/usuario/carrito",
            type: "GET",
            async: true,
            data: {},
            headers: {'Authorization': cabecera },
            dataType: "json",
            success: function (datosCarrito) {
                var cantidadPed = "<span id=\"cantidadPedidos\">"+datosCarrito.pedidos.length+"</span>";


                    $("#cantidadPedidos").replaceWith(cantidadPed);
                    $("#botonCarrito").click(function (){
                        var precioTotal=0;
                        var contenidoTabla="";
                        $.each(datosCarrito.pedidos, function () {
                            contenidoTabla += "<tr><td>"+this.producto.nombre+"</td><td>"+this.producto.categoria.nombre+"</td><td>"+this.unidades+"</td><td>"+this.producto.precio+"</td></tr>"
                            precioTotal += (this.producto.precio)*(this.unidades);
                        });
                        var tablaCarrito;
                        var cabeceraTabla = "<div class=\"w3-row w3-grayscale\" id=\"productos\"><table><tr><th>Nombre</th><th>Categoria</th><th>Unidades</th><th>Precio</th></tr>";
                        var finalTabla = "<tr><td colspan=\"3\"><b>Precio Total</b></td><td><b>"+precioTotal+"€</b></td></tr></table><div><input type='button' id='botonCompraCarrito' name='botonCompraCarrito' value='Comprar Carrito' ></div></div>";
                        tablaCarrito = cabeceraTabla+contenidoTabla+finalTabla;
                        $("#productos").replaceWith(tablaCarrito);
                        $("#botonCompraCarrito").click(function (){comprarCarrito(datosCarrito.id);});
                    });


            }
        }
    );
}
function comprarCarrito(idCarrito){
    var cabecera = 'Bearer '+token;

    $.ajax(
        {
            url: "http://api_tienda.local/api/carrito/"+idCarrito,
            type: "PUT",
            async: true,
            data: JSON.stringify({ _method: 'PUT', abierto:false}),
            headers: {'Authorization': cabecera },
            dataType: "json",
            success: function (datosPedido) {
                $("#cantidadPedidos").text(0);
                alert("Carrito comprado con exito.");
                var vacio = "<div class=\"w3-row w3-grayscale\" id=\"productos\"></div>";
                $("#productos").replaceWith(vacio);

                cargarProductos();


            }
        }
    );
}

