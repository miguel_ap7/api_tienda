<?php

namespace App\Controller;

use App\Entity\Categoria;
use App\Entity\Producto;
use App\Repository\ProductoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;


class ProductoController extends AbstractController
{
    /**
     * @var ProductoRepository
     */


    public function __construct(ProductoRepository $ProductoRepository){
        $this->ProductoRepository = $ProductoRepository;
    }

    /**
     * @Route("/api/producto/{id}", name="listarUnProducto", methods={"GET"})
     */
    public function getProducto($id): Response
    {
        $producto = $this->getDoctrine()->getRepository(Producto::class)->findOneBy(['id' => $id]);

        if(!$producto){
            $data = [
                'status' => 404,
                'errors' => "Producto no encontrado",
            ];
            return new JsonResponse($data, 404);
        }else{
            $data = [
                'id' => $producto->getId(),
                'nombre' => $producto ->getNombre(),
                'precio' => $producto->getPrecio(),
                'stock' => $producto->getStock(),
                'categoria' => $producto->getCategoria()->getNombre(),
                'photoUrl' => $this->getParameter("url_images").$producto->getPhotoUrl(),
            ];
        }


        return new JsonResponse($data, Response::HTTP_OK);
    }
    /**
     * @Route("/api/producto/{id}", name="update_Producto", methods={"PUT"})
     */
    public function update($id, Request $request): JsonResponse
    {
        $producto = $this->ProductoRepository->findOneBy(['id' => $id]);
        $data = json_decode($request->getContent(),true);
        if(!$producto){
            $data = [
                'status' => 404,
                'errors' => "Producto no encontrado",
            ];
            return new JsonResponse($data, 404);
        }else{
            empty($data['nombre']) ? true : $producto->setNombre($data['nombre']);
            empty($data['precio']) ? true : $producto->setPrecio($data['precio']);
            empty($data['stock']) ? true : $producto->setStock($data['stock']);
            empty($data['idCategoria']) ? true : $producto->setCategoria($this->getDoctrine()->getRepository(Categoria::class)->find($data['idCategoria']));

            $updateProducto = $this->ProductoRepository->updateProducto($producto);
        }




        return new JsonResponse(['status'=> 'Producto modificado!'], Response::HTTP_OK);
    }
    /**
     * @Route("/api/producto", name="crear_Producto", methods={"POST"})
     */
    public function producto(Request $request): JsonResponse
    {
        try{
        $data = json_decode($request->getContent(),true);
        $nombre = $data['nombre'];
        $precio = $data['precio'];
        $stock = $data['stock'];
        $categoria = $data['categoria'];

        if (empty($nombre) || empty($precio) || empty($stock) || empty($categoria) ){
            throw new NotFoundHttpException('Se tiene que introducir todos los parametros');
        }
        $this->ProductoRepository->crearProducto($nombre, $precio, $stock, $categoria);

        return new JsonResponse(['status'=> 'Producto creado!'], Response::HTTP_CREATED);
        }catch (\Exception $e){
            $data = [
                'status' => 422,
                'errors' => "Datos no validos",
            ];
            return new JsonResponse($data, 422);

        }

    }

    /**
     * @Route("/api/producto/{id}", name="borrar_Producto", methods={"DELETE"})
     */
    public function delProducto($id): JsonResponse
    {

        $producto = $this->ProductoRepository->findOneBy(['id' => $id]);
        if (!$producto){
            $data = [
                'status' => 404,
                'errors' => "Producto no encontrado",
            ];
            return new JsonResponse($data, 403);
        }

        $this->ProductoRepository->borrarProducto($producto);

        return new JsonResponse(['status'=> 'Producto borrado!'], Response::HTTP_OK);

    }



}
