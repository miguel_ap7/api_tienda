<?php

namespace App\Controller;

use App\Entity\Categoria;
use App\Entity\ListaPedidoProducto;
use App\Entity\Pedido;
use App\Entity\Producto;
use App\Entity\Usuario;
use App\Repository\ListaPedidoProductoRepository;
use App\Repository\ProductoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Routing\Annotation\Route;



class ListaPedidoProductoController extends AbstractController
{
    /**
     * @var ListaPedidoProductoRepository
     */


    public function __construct(ListaPedidoProductoRepository $listaPedidoProductoRepository){
        $this->ListaPedidoProductoRepository = $listaPedidoProductoRepository;
    }

    /**
     * @Route("/api/carrito", name="listarCarritos", methods={"GET"})
     */
    public function getAllCarritos(): JsonResponse
    {
        $listasPedidoProducto = $this->getDoctrine()->getRepository(ListaPedidoProducto::class)->findAll();
        foreach ($listasPedidoProducto as $listaPedidoProducto){
            $data [] = [
                'id' => $listaPedidoProducto->getId(),
                'idUsuario' => $listaPedidoProducto ->getUsuario()->getId(),
                'abierto' => $listaPedidoProducto ->getAbierto(),
            ];
        }


        return new JsonResponse($data, Response::HTTP_OK);
    }
    /**
     * @Route("/api/carrito/{id}", name="listarUnCarrito", methods={"GET"})
     */
    public function getCarrito($id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user= $this->getUser();
        $listaPedidoProducto = $this->getDoctrine()->getRepository(ListaPedidoProducto::class)->findOneBy(['id' => $id]);
        $pedidosUser = $this->getDoctrine()->getRepository(Pedido::class)->findBy(array('usuario' => $user));
        $allCategorias = $this->getDoctrine()->getRepository(Categoria::class)->findAll();

        return $this->enviaSerializado($listaPedidoProducto);

    }




    /**
     * @Route("/api/carrito/{id}", name="update_Carrito", methods={"PUT"})
     */
    public function update($id, Request $request): Response
    {
        $listaPedidoProducto = $this->ListaPedidoProductoRepository->findOneBy(['id' => $id]);
        $data = json_decode($request->getContent(),true);
        if(!$listaPedidoProducto){
            $data = [
                'status' => 404,
                'errors' => "Carrito no encontrado",
            ];
            return new Response($data, 404);
        }else{
            if (isset($data['abierto'])) {
                $listaPedidoProducto->setAbierto($data['abierto']);
            };
            //$listaPedidoProducto->setAbierto($data['abierto']);
            $listaPedidoProductoActualizado = $this->ListaPedidoProductoRepository->updateCarrito($listaPedidoProducto);
        }



        return $this->enviaSerializado($listaPedidoProductoActualizado);
        //return new JsonResponse($listaPedidoProductoActualizado, Response::HTTP_OK);
    }
    /**
     * @Route("/api/carrito", name="crear_Carrito", methods={"POST"})
     */
    public function carrito(Request $request): JsonResponse
    {
        try{
            $data = json_decode($request->getContent(),true);
            $idUsuario = $data['idUsuario'];

            if (empty($idUsuario) ){
                throw new NotFoundHttpException('Se tiene que introducir todos los parametros');
            }
            $this->ListaPedidoProductoRepository->crearCarrito($idUsuario);

            return new JsonResponse(['status'=> 'Carrito creado!'], Response::HTTP_CREATED);
        }catch (\Exception $e){
            $data = [
                'status' => 422,
                'errors' => "Datos no validos",
            ];
            return new JsonResponse($data, 422);

        }

    }

    /**
     * @Route("/api/carrito/{id}", name="borrar_Carrito", methods={"DELETE"})
     */
    public function delCarrito($id): JsonResponse
    {

        $listaPedidoProducto = $this->ListaPedidoProductoRepository->findOneBy(['id' => $id]);
        if (!$listaPedidoProducto){
            $data = [
                'status' => 404,
                'errors' => "Producto no encontrado",
            ];
            return new JsonResponse($data, 403);
        }

        $this->ListaPedidoProductoRepository->borrarCarrito($listaPedidoProducto);

        return new JsonResponse(['status'=> 'Producto borrado!'], Response::HTTP_OK);

    }
    public function enviaSerializado($objetoASerializar):Response{
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object;
            },
        ];

        $normalizer = new ObjectNormalizer(null, null, null, null, null, null, $defaultContext);

        // construimos o encoder; neste caso JSON
        $encoder= new JsonEncoder();

        // construimos o serializer co normalizer e encoder
        $serializer = new Serializer(array($normalizer), array($encoder));

        // serializamos en JSON
        $jsonContent =  $serializer->serialize($objetoASerializar, "json");

        // Construimos a resposta serializada en JSON
        $response = new Response();
        $response ->headers->set("Content-Type", "application/json");
        $response->setStatusCode(200);
        $response->setContent($jsonContent);

        // devolvemos a resposta
        return $response;

    }

}
