<?php

namespace App\Repository;

use App\Entity\ListaPedidoProducto;
use App\Entity\Pedido;
use App\Entity\Producto;
use App\Entity\Usuario;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method Pedido|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pedido|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pedido[]    findAll()
 * @method Pedido[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PedidoRepository extends ServiceEntityRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Pedido::class);
        $this->manager = $manager;
    }

    public function updatePedido(Pedido $pedido): Pedido{
        $this->manager->persist($pedido);
        $this->manager->flush();

        return $pedido;
    }

    public function crearPedido($idProducto, $idListaPedidoProducto, $idUsuario, $unidades)
    {
        $newPedido = new Pedido();
        $newPedido
            ->setUnidades($unidades);

        $producto = $this -> manager -> getRepository(Producto::class)->find($idProducto);
        $newPedido ->setProducto($producto);

        $usuario = $this -> manager -> getRepository(Usuario::class)->find($idUsuario);
        $newPedido ->setUsuario($usuario);


        $listaPedidoProducto = $this -> manager -> getRepository(ListaPedidoProducto::class)->find($idListaPedidoProducto);
        $newPedido ->setListaPedidoProducto($listaPedidoProducto);

        $this->manager->persist($newPedido);
        $this->manager->flush();
    }

    public function borrarPedido($pedido){
        $this->manager->remove($pedido);
        $this->manager->flush();
    }

    // /**
    //  * @return Pedido[] Returns an array of Pedido objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Pedido
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
